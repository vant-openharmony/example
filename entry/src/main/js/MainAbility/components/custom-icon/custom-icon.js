const map = {
  "wechat": "\ue613",
  "alipay": "\ue678",
  "baidu": "\ue652",
  "logout": "\ue673",
  "user": "\ue68d"
};

export default {
  props: {
    name: String,
    color: String,
    size: {
      default: 32,
    },
  },
  computed: {
    content() {
      return map[this.name];
    },
  },
  handClick() {
    this.$emit("tap");
  },
};
