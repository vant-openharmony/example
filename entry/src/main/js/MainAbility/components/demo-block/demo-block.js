export default {
    props: {
        title: String,
        card: {
            type: Boolean,
            default: true,
        }
    }
}
