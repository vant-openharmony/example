import dconvert from 'dconvert-array';
import {basic,outline,filled} from './config.js';

export default {
    data() {
       return {
           list: [dconvert(basic,3),dconvert(outline,3),dconvert(filled,3)],
       }
    }
}
