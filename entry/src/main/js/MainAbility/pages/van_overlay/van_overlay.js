import prompt from '@system.prompt';

export default {
    data: {
        show: false,
        show1: false,
    },

    onClickShow() {
        this.$set('show', true);
    },

    onClickHide() {
        this.$set('show', false);
    },

    onClickShow1() {
        this.$set('show1', true);
    },

    onClickHide1() {
        this.$set('show1', false);
    },
}
