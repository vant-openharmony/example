const originalData = {
    show: false,
    round: false,
    closeable: false,
    width: undefined,
    height: '100px',
    closeIcon: 'cross',
    closeIconPosition: 'top-right',
    position: 'center',
}

export default {
    data: {
        ...originalData,
    },

    setData(data) {
        var _this = this;
        Object.keys(data).forEach((k) => {
            _this.$set(k, data[k]);
        });
        setTimeout(() => {
            this.$set('show', true);
        }, 66);
    },

    showPopup() {
        this.setData({ ...originalData })
    },

    showTopPopup() {
        this.setData({ ...originalData, position: 'top', height: '30%' })
    },
    showBottomPopup() {
        this.setData({ ...originalData, position: 'bottom', height: '30%' })
    },
    showLeftPopup() {
        this.setData({ ...originalData, position: 'left', width: '200px', height: '100%' })
    },
    showRightPopup() {
        this.setData({ ...originalData, position: 'right', width: '200px', height: '100%' })
    },

    onClose() {
        console.log('onClose');
        this.$set('show', false);
    },
};